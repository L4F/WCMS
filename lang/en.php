<?php
$lang = array();
$lang['DIG'] = 'Dig';
$lang['DUGG'] = 'Dugg';
$lang['DIGIT'] = 'Digit';
$lang['JOIN'] = 'Join';
$lang['LOGIN'] = 'Login';
$lang['LOGOUT'] = 'Logout';
$lang['SIGNUP'] = 'Signup';
$lang['SUBMIT'] = 'Submit';
$lang['SEARCH'] = 'Search';
$lang['SAVE'] = 'Save';
$lang['DEL'] = 'Del';
$lang['MOVE'] = 'Move';
$lang['COPY'] = 'Copy';
$lang['PASS'] = 'Pass';
$lang['REFUSE'] = 'Refuse';
$lang['SELECTALL'] = 'SelectAll';
$lang['RECYCLE'] = 'Recycle';
$lang['PREVIOUS'] = 'Previous';
$lang['UPLOAD'] = 'Upload';
$lang['RESET'] = 'Reset';
$lang['AUTHORIZE'] = 'Authorize';
$lang['BIND'] = 'Bind';
$lang['CODEIMG']='Code';
$lang['UPDATE'] = 'Update';
$lang['CHECK']='CHECK';

//系统后台内容 回复

$lang['BASE']='Base';
$lang['SHOW']='Show';
$lang['HIDDEN']='Hidden';
$lang['WEIGHT']='Weight';
$lang['TITLECOLOR']='TitleColor';
$lang['VIEWS']='Views';
$lang['TITLE'] = 'Title';
$lang['SUBTITLE'] = 'SubTitle';
$lang['KEYWORD'] = 'Keyword';
$lang['ID'] = 'ID';
$lang['CATE'] = 'Cate';
$lang['PUBTIME'] = 'Pubtime';
$lang['AUTHOR'] = 'Author';
$lang['ACTION'] = 'Action';
$lang['SORT'] = 'Sort';
$lang['MOBILE'] = 'Mobile';
$lang['USERNAME'] = 'Username';
$lang['CONTENT'] = 'Content';
$lang['RANGE'] = 'Range';
$lang['TYPES'] = 'Types';
$lang['NOREAD'] = 'Message';
$lang['READ'] = 'Read';
$lang['THUMB']='Thumb';
$lang['COUNTRY']='Country';
$lang['CITY']='City';
$lang['EMAIL']='Email';
$lang['STATUS']='Status';
$lang['SUMMARY']='Summary';
$lang['ALL']='All';
$lang['PREMISSION']='Premission';
$lang['FLAG']='Flag';
$lang['PIC']='Source';
$lang['ADDNOTICE']='if checked Source or width and height is disabled';
$lang['EXTEND']='Extend';
$lang['TOOL']='Tool';
$lang['GROUP']='Group';
$lang['LOGINTIME']='LoginTime';
$lang['ACCOUNT']='Account';
$lang['REMARK']='Remark';
$lang['PASSWORD'] = 'Password';
$lang['COUPONS'] = 'Coupons';
$lang['SPECPAGE'] = 'Special Page';
$lang['LISTPAGE'] = 'List Page';
$lang['CONTENTPAGE'] = 'Content Page';
$lang['CURRENT'] = 'Current';
$lang['NAME'] = 'Name';
$lang['VALUE'] = 'Value';
$lang['STATIC'] = 'Static';
$lang['SUCCESS'] = 'Success';
$lang['FAILED'] = 'Failed';
$lang['FORWARD']='Forward';
$lang['NUMBER']='Number';
$lang['MANAGER']='Manager';

//系统配置信息
$lang['sys']['pagenum'] = 'PageNum';
$lang['sys']['newsnum'] = 'CateNewsNum';
$lang['sys']['thumbs'] = 'ThumbsSize';
$lang['sys']['shortcut'] = 'ShortCut';
$lang['sys']['author']='Author';
$lang['sys']['smarty'] = 'Smarty';
$lang['sys']['iscahce'] = 'Caching';
$lang['sys']['cachetime'] = 'CacheTime';
$lang['sys']['compiled'] = 'Compiled';
$lang['sys']['sys'] = 'System';
$lang['sys']['php'] = 'php';
$lang['sys']['cgi'] = 'CGI';
$lang['sys']['phppath'] = 'PHP Path';
$lang['sys']['ip']='SERVER IP';
$lang['sys']['strpos'] = 'Strpos';
$lang['sys']['error'] = 'ERROR';
$lang['sys']['extend']='PHP Extend';
$lang['sys']['timezone'] = 'TimeZone';
$lang['sys']['session'] = 'Session';
$lang['sys']['sessionpath'] = 'Session Path';
$lang['sys']['pdo'] = 'PDO';
//系统后台菜单语言


$lang['news']['listing']  = 'Content';
$lang['news']['config'] = 'System';
$lang['news']['addcon'] = 'Add';
$lang['news']['clist']= 'Comment';
$lang['member']['loginlog'] = 'Loginlog';
$lang['member']['add'] = 'AddMember';
$lang['member']['repassword'] = 'Repassword';
$lang['news']['attr'] = 'Attr';
$lang['news']['moreattr'] = 'Moreattr';
$lang['news']['bindcate']= 'Bindcate';
$lang['news']['flag']= 'Flag';
$lang['news']['templist'] = 'Templist';
$lang['member']['listing'] = 'Member';
$lang['news']['editcategory'] = 'Category';
$lang['file']['index'] = 'FileManager';
$lang['order']['listing'] = 'OrderListing';
$lang['news']['batchall'] = 'Static';
$lang['vote']['config'] = 'VoteConfig';

//前台导航
$lang['NAV_INDEX'] = 'INDEX';
$lang['NAV_PRODUECT'] = 'PRODUCT';
$lang['NAV_ABOUT'] = 'About US';
$lang['NAV_CER'] = 'CERTIFICATE';
$lang['NAV_CONTACT'] = 'CONTACT';
//前台留言表单
$lang['FORM_USERNAME']='Username';
$lang['FORM_MOBILE']='Mobile';
$lang['FORM_EMAIL']='Email';
$lang['FORM_CONTENT']='Content';

//产品栏目多语言
$lang[10]='Eye Shadow';
$lang[11]='Blush';
$lang[12]='Powder';
$lang[13]='Foundation';
$lang[14]='Gloss';
$lang[15]='Lipstick';
$lang[16]='Lip Balm';
$lang[17]='Eyeliner';
$lang[18]='Mascara';
$lang[19]='Eyeliner pencil';
$lang[20]='Makeup Palette';
$lang[21]='Nails';
$lang[22]='Glue';
$lang[24]='Beauty tools';


//前台产品库
$lang['PRO_TITLE']='Pro_title';
$lang['PRO_TYPES']='Pro_types';
$lang['PRO_PRICES']='Pro_prices';
$lang['PRO_COLORS']='Pro_colors';
$lang['PRO_DETAIL']='Pro_detall';