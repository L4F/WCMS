<script type="text/javascript"
	src="./static/public/jquery-1.11.0.min.js"></script>
<script src="./static/public/jquery-ui.js" type="text/javascript"
	charset="utf-8"></script>
<script src="./static/public/evol.colorpicker.min.js"
	type="text/javascript" charset="utf-8"></script>

<link href="./static/public/css/evol.colorpicker.css" rel="stylesheet"
	type="text/css">
<link href="./static/public/css/jquery-ui.css" rel="stylesheet"
	type="text/css">
	<link href="./static/bootstrap2/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
	<script type="text/javascript"
	src="./static/bootstrap2/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript"
	src="./static/bootstrap2/js/bootstrap-datetimepicker.zh-CN.js"
	charset="UTF-8"></script>

    <script type="text/javascript" src="./static/public/layer/layer.min.js" ></script>

{literal}
<style>
.cke_button__addpic{background: url(./static/public/ckeditor/plugins/addpic/addpic.png) no-repeat 5px 5px !important;}

.table th, .table td {
border-top:none;
border-bottom: 1px solid #f1f1f1;
</style>
<script type='text/javascript'>

$(document).ready(function(){
  
    $("#mycolor").colorpicker({
        color: "#ffc000",     
        history: false,
        displayIndicator: false
        });
    $("#mycolor").on("change.color", function(event, color){
        $('#title').css('color', color);
        var x=$("#b").attr("class");
        if(x=="b"){
            $("input[name='css']").val('color:'+color+";font-weight:bold;");
        }else{
          	 $("input[name='css']").val('color:'+color);
        }
    })
   });

			

			function apd(t){
    var htm='<input type="text" name='+t+'[]>';
     $("#"+t).append(htm);
			}
			function show(){
             $("#kuozhan").show();
			}
			function jiacu(){
				var c=$("#mycolor").val();
				if(c!="#ffc000"){
                    c="color:"+c+";";
				}else{
                    c=""; 
				}
             
               var x=$("#b").attr("class");
               if(x=="b"){
            	   $("#title").css("font-weight","");
                   $("#b").removeClass("b");
                   $("input[name='css']").val(c);
               }else{             
               $("#title").css("font-weight","bold");
               $("#b").addClass("b");
               $("input[name='css']").val(c+"font-weight:bold;");
               }
               
			}
			function checktitle(){
				   var t=$("#title").val();
				   $.post("./index.php?factory/search",{key:"title",value:t,datatype:"json"},function(result){
					   if(result.status=="success"){
						   $("#checktitle").addClass("alert alert-success");
						   }else{
							   $("#checktitle").addClass("alert alert-error");
							   }
				var str="<ul>";
						 for(var i=0;i<result.data.length;i++){
				  str+="<li>"+result.data[i].title+"</li>";
					 }
						 str=str+"</ul>";
				    $("#checktitle").html(str)
				   },"json")
				
				}
		</script>


{/literal} 
<div id="content" >


<div class="row-fluid">




			<div class="well"> <!-- Only required for left/right tabs -->
  <ul class="nav nav-tabs suoding">
    <li><a href="javascript:history.go(-1)">« 返回</a></li>
    <li class="active"><a href="#tab1" data-toggle="tab">基本</a></li>
    <li><a href="#tab2" data-toggle="tab">权限</a></li>
    <li><a href="#tab3" data-toggle="tab">扩展</a></li>
   </ul>
  <div class="tab-content">
   <div class="tab-pane active" id="tab1">


<form action="./index.php?factory/save" method="post" enctype="multipart/form-data"
	class="form-inline"><input type="hidden" name="p" value="{$p}"><!-- 存储上一级页数 -->
<input type="hidden" name="preid" value="{$preid}"><!-- 存储上一级页数 --> <!-- 内容// -->

<table class="table">

	<tr>
		<td class="span2">标题</td>
		<td>
		<input type="hidden" name="css" value="{$news.css}" />
<span class="input-append">
<input type="text" name="title" value="{$news.title}" style="{$news.css}" class="input-xxlarge rule" id="title">
<a class="btn" href="javascript:checktitle()" >重复</a>
<a class="btn" href="javascript:jiacu();"><i class="icon-bold"></i></a>
 <a class="btn"><input type="hidden" id="mycolor"
			class="colorPicker evo-cp0" /></a>

</span>			

			<div  id="checktitle"></div>

		</td>
		<td>
		<div id="b"></div>
		</td>

	</tr>
	
		<tr>
		<td>副标题</td>
		<td>

<input type="text" name="subtitle"  class="input-xlarge" id="title" value="{$news.subtitle}">
			


	

		</td>
		<td rowspan=4 style="width:180px;">
		<div>
	<img src="{$news.thumb}" class="img-polaroid" style="width:150px;height:150px;">
		</div>
		</td>
	
	</tr>
	<tr>
		<td >分类</td>
		<td>
		<div>{$category}</div>
		
		</td>

	</tr>

	<tr>
		<td>标签</td>
		<td>{foreach from=$flag item=g} 
		{$g.name}:
		{foreach from=$g.data item=l}
		<label class="checkbox">
		 <input type="checkbox" value="{$l.id}" name="flag[]"
			{if $l.ischecked==true}checked{/if}>{$l.name} 
	</label>	{/foreach}
	<br>
	{/foreach}
	</td>
	
	</tr>

	<tr>
		<td>缩略图</td>
		<td>
		<div class="uploader" id="uniform-fileInput"><input
			class="input-file uniform_on" name="thumb" id="fileInput" type="file"><span
			class="filename" style="-webkit-user-select: none;">No file selected</span><span
			class="action" style="-webkit-user-select: none;">上传</span></div>
		<input size="3" type="text" class="input-mini" maxlength="3"
			name="width" value="{if $width!=''}{$width}{else}360{/if}"> x <input
			type="text" class="input-mini" maxlength="3" size="3" name="height"
			value="{if $height!=''}{$height}{else}360{/if}"> 或 
			<label class="checkbox">
			<input
			type="checkbox" name="same">原图</label></td>
		

	</tr>