<?php
interface IGoods {
	
	function getGoodsById($id);
	function getGoodsPriceById($id);
}